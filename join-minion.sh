#!/bin/sh

SCRIPTDIR=`dirname "$0"`
. $SCRIPTDIR/cluster-config
node_ip=`hostname -i`
token=$1

echo "#"
echo "# Add route to service network"
echo "#"

export private_dev=`ip route | grep "src $node_ip" | cut -d ' ' -f 3`
route add -net $SERVICE_SUBNET $private_dev

kubeadm join --token $token --discovery-token-unsafe-skip-ca-verification $API_SERVER_ADDRESS:$API_SERVER_PORT
