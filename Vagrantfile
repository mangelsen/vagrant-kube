# -*- mode: ruby -*-
# vi: set ft=ruby :

nodes = {
  'master' => '192.168.60.2',
  'minion-0' => '192.168.60.3',
  'minion-1' => '192.168.60.4',
}

Vagrant.configure("2") do |config|
  config.vm.box = "bento/centos-7.4"

  nodes.each do |name, ip|
    name = "#{name}"
    config.vm.define name  do |node|
      node.vm.hostname = name
      node.vm.network :private_network, ip: ip, virtualbox__intnet: "vagrant-kube"
      node.vm.provision :shell, inline: <<-SHELL
        echo "127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4" > /etc/hosts
        echo "::1         localhost6 localhost6.localdomain6" >> /etc/hosts
        echo "#{nodes.map{|n, ip| "#{ip} #{n}"}.join("\n")}" >> /etc/hosts
        systemctl restart network
        sh /vagrant/prepare-node.sh
      SHELL
      if name == "master"
        node.vm.network "forwarded_port", guest: 8001, host: 8001
        node.vm.network "forwarded_port", guest: 80, host: 8080
        node.vm.network "forwarded_port", guest: 443, host: 8443
        node.vm.provision :shell, inline: <<-SHELL
          sh /vagrant/init-master.sh qthk5f.yiwlqpsfcyrjfz4g
        SHELL
      end
      if name =~ /minion-[0-9]*/
        node.vm.provision :shell, inline: <<-SHELL
          sh /vagrant/join-minion.sh qthk5f.yiwlqpsfcyrjfz4g
        SHELL
      end
    end
  end

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  config.vm.provider "virtualbox" do |vb|
    # Customize the amount of memory on the VM:
    vb.memory = "4096"
    vb.cpus = "2"
  end
end
