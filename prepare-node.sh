#!/bin/sh

SCRIPTDIR=`dirname "$0"`
. $SCRIPTDIR/cluster-config

echo "#"
echo "# Turn off swap and selinux"
echo "#"

swapoff -a
setenforce 0

if test -n "$HTTP_PROXY_HOST"; then
  echo "#"
  echo "# Setup local http proxy proxy"
  echo "#"

  if test "$HTTP_PROXY_USER"; then
    HTTP_PROXY_AUTH="$HTTP_PROXY_USER:$HTTP_PROXY_PASSWORD@"
  fi
  REMOTE_HTTP_PROXY="http://$HTTP_PROXY_AUTH$HTTP_PROXY_HOST:$HTTP_PROXY_PORT"
  CNTLM_RPM_FILE="cntlm-$CNTLM_VERSION-$CNTLM_RELEASE.x86_64.rpm"
  curl -sS -x $REMOTE_HTTP_PROXY -Lo $CNTLM_RPM_FILE \
    "https://sourceforge.net/projects/cntlm/files/cntlm/cntlm%20$CNTLM_VERSION/$CNTLM_RPM_FILE/download"
  yum install -y $CNTLM_RPM_FILE

  cat > /etc/cntlm.conf << EOF
Proxy $HTTP_PROXY_HOST:$HTTP_PROXY_PORT
Domain $HTTP_PROXY_DOMAIN
NoProxy localhost, 127.0.0.*, 10.*, 192.168.*, $HTTP_PROXY_EXCLUDE
Listen 3128
EOF
  if test -n "$HTTP_PROXY_USER"; then
    echo "Username $HTTP_PROXY_USER" >> /etc/cntlm.conf
  fi
  if test -n "$HTTP_PROXY_PASSWORD"; then
    echo "Password $HTTP_PROXY_PASSWORD" >> /etc/cntlm.conf
  fi

  systemctl enable cntlmd && systemctl start cntlmd

  echo "proxy=http://localhost:3128" >> /etc/yum.conf
fi

echo "#"
echo "# Install and start docker"
echo "#"

yum install -y docker
sed -i "s/--selinux-enabled //g" /etc/sysconfig/docker
if test -n "$HTTP_PROXY_HOST"; then
  cat >> /etc/sysconfig/docker << EOF
HTTP_PROXY=http://localhost:3128
HTTPS_PROXY=http://localhost:3128
http_proxy=http://localhost:3128
https_proxy=http://localhost:3128
EOF
fi
systemctl enable docker && systemctl start docker


echo "#"
echo "# Install kubelet, kubectl und kubeadm"
echo "#"

cat <<'EOF' > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-$basearch
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
yum install -y kubelet-$KUBERNETES_VERSION kubeadm-$KUBERNETES_VERSION kubectl-$KUBERNETES_VERSION

echo "#"
echo "# Start kubelet, kubectl und kubeadm"
echo "#"

systemctl enable kubelet && systemctl start kubelet


echo "#"
echo "# Tweak network settings"
echo "#"

cat <<EOF >  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
sysctl --system
