#!/bin/sh

SCRIPTDIR=`dirname "$0"`
. $SCRIPTDIR/cluster-config
node_ip=`hostname -i`
token=$1

if test -n "$HTTP_PROXY_HOST"; then
  PROXY="-xhttp://localhost:3128"
fi

echo "#"
echo "# Init Kubernetes master node with kubeadm"
echo "#"

kubeadm init --token $token \
  --apiserver-advertise-address $API_SERVER_ADDRESS  --apiserver-bind-port $API_SERVER_PORT \
  --service-cidr $SERVICE_SUBNET --kubernetes-version $KUBERNETES_VERSION


echo "#"
echo "# Create Weave Net overlay networks for pods"
echo "#"
echo "# See also https://www.weave.works/docs/net/latest/kubernetes/kube-addon/"
echo "#"

export KUBECONFIG=/etc/kubernetes/admin.conf
export kubever=$(kubectl --kubeconfig /etc/kubernetes/admin.conf version | base64 | tr -d '\n')
curl $PROXY -sSLo /etc/kubernetes/manifests/weave-net.yml "https://cloud.weave.works/k8s/net?k8s-version=$kubever"
kubectl apply -f /etc/kubernetes/manifests/weave-net.yml


echo "#"
echo "# Deploy Heapster and Kubernetes dashboard"
echo "#"

curl $PROXY -sSLo /etc/kubernetes/manifests/heapster-rbac.yaml "https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/rbac/heapster-rbac.yaml"
kubectl apply -f /etc/kubernetes/manifests/heapster-rbac.yaml
curl $PROXY -sSLo /etc/kubernetes/manifests/heapster-controller.yaml "https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/standalone/heapster-controller.yaml"
kubectl apply -f /etc/kubernetes/manifests/heapster-controller.yaml
curl $PROXY -sSLo /etc/kubernetes/manifests/kubernetes-dashboard.yaml "https://raw.githubusercontent.com/kubernetes/dashboard/v1.8.3/src/deploy/recommended/kubernetes-dashboard.yaml"
kubectl apply -f /etc/kubernetes/manifests/kubernetes-dashboard.yaml
cat << EOF > /etc/kubernetes/dashboard-admin.yaml
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: kubernetes-dashboard
  labels:
    k8s-app: kubernetes-dashboard
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: kubernetes-dashboard
  namespace: kube-system
EOF
kubectl create -f /etc/kubernetes/dashboard-admin.yaml


echo "#"
echo "# Deploy Nginx ingress controller"
echo "#"
echo "# See also https://github.com/kubernetes/ingress-nginx"
echo "#"

curl $PROXY -sSL https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/namespace.yaml | kubectl apply -f -
curl $PROXY -sSL https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/default-backend.yaml | kubectl apply -f -
curl $PROXY -sSL https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/configmap.yaml | kubectl apply -f -
curl $PROXY -sSL https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/tcp-services-configmap.yaml | kubectl apply -f -
curl $PROXY -sSL https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/udp-services-configmap.yaml | kubectl apply -f -
curl $PROXY -sSL https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/rbac.yaml | kubectl apply -f -
kubectl apply -f /vagrant/ingress-daemonset.yml

echo "#"
echo "# Start API server proxy"
echo "#"
cat > /etc/systemd/system/kube-api-proxy.service << EOF
[Unit]
Description=Local Kubernetes API Server Proxy
Requires=network-online.target
After=network-online.target

[Service]
Restart=on-failure
ExecStart=/bin/kubectl --kubeconfig /etc/kubernetes/admin.conf proxy --address 10.0.2.15 --port 8001
KillSignal=SIGINT
EOF

systemctl daemon-reload
systemctl enable kube-api-proxy && systemctl start kube-api-proxy
