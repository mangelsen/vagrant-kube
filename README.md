# A simple Kubernetes cluster backed by vagrant

Simply type

```
vagrant up
```

to create the cluster. Then access Dashboard at:

```
http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/
```

Adjust your hardware (number of minions, cpu, ram etc.) in `Vagrantfile`. The
software configuration (Kubernetes version etc.) can be found in
`cluster-config`.

To access the running cluster via CLI, do

```bash
# 1. Download kubectl (replace 1.10.0 with the KUBERNETES_VERSION from file cluster-config)
curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.10.0/bin/linux/amd64/kubectl

# Use the following instead, if you are on a Mac
# curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.10.0/bin/darwin/amd64/kubectl

chmod +x ./kubectl
./kubectl -s localhost:8001 get nodes
./kubectl -s localhost:8001 get pods --all-namespaces
./kubectl -s localhost:8001 ...
```

The ingress controller is listening on localhost port 8080 (HTTPS 8443). The
following command

```
curl http://localhost:8080
```

will output a HTPP status code 404 from the default backend. Use

```
curl http://localhost:8080/healthz
```

to call the default backend's health check.

# Running behind a http proxy

(1) Uncomment the  `HTTP_PROXY_*` section in `cluster-config`:
server:

```bash
...

HTTP_PROXY_HOST=<Hostname or ip adress of your proxy server>
HTTP_PROXY_PORT=<port number of your proxy server>
HTTP_PROXY_DOMAIN=<AD domain of your proxy server>
HTTP_PROXY_USER=user
HTTP_PROXY_PASSWORD=password
HTTP_PROXY_EXCLUDE="*.yoyodine.com, *.my-domain, 176.10.*"
```

(2) Make sure Vagrant can download boxes when you run it:

```bash
http_proxy=proto://proxy:port https_proxy=proto://proxy:port vagrant up
```

If your http proxy is listening on your local machine's loopback device
(`127.0.0.1`), use `HTTP_PROXY_HOST=10.0.2.2`.

Make sure, that Vagrant does _not_ touch the guest machines proxy settings.
Normally that means, that you should not use the Vagrent plugin called `proxyconf`.

# Try some examples

The following manifest creates to deployments, tea and coffe, with the relevant
services and an ingress definition.

```yaml
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: coffee
spec:
  replicas: 2
  selector:
    matchLabels:
      app: coffee
  template:
    metadata:
      labels:
        app: coffee
    spec:
      containers:
      - name: coffee
        image: nginxdemos/hello:plain-text
        ports:
        - containerPort: 80
---
apiVersion: v1
kind: Service
metadata:
  name: coffee-svc
spec:
  ports:
  - port: 80
    targetPort: 80
    protocol: TCP
    name: http
  selector:
    app: coffee
---
apiVersion: extensions/v1beta1
kind: Deployment
metadata:
  name: tea
spec:
  replicas: 3
  selector:
    matchLabels:
      app: tea
  template:
    metadata:
      labels:
        app: tea
    spec:
      containers:
      - name: tea
        image: nginxdemos/hello:plain-text
        ports:
        - containerPort: 80
---
apiVersion: v1
kind: Service
metadata:
  name: tea-svc
  labels:
spec:
  ports:
  - port: 80
    targetPort: 80
    protocol: TCP
    name: http
  selector:
    app: tea
---
apiVersion: extensions/v1beta1
kind: Ingress
metadata:
  name: cafe-ingress
spec:
  rules:
  - host: www.k8s-example.com
    http:
      paths:
      - path: /tea
        backend:
          serviceName: tea-svc
          servicePort: 80
      - path: /coffee
        backend:
          serviceName: coffee-svc
          servicePort: 80
```

Use

```
curl -H "Host: www.k8s-example.com" http://localhost:8080/tea
curl -H "Host: www.k8s-example.com" http://localhost:8080/coffee
```

to see the example working.

# Usefull links

- https://serverfault.com/questions/78240/debugging-rules-in-iptables
- http://www.opensourcerers.org/how-to-trace-iptables-in-rhel7-centos7/

# Things to do ...

- Heapster Version festlegen
- Dashboard Version festlegen
